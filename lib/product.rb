class Product
  attr_reader :id, :price
  def initialize(id, name, price)
    @id = id
    @name = name
    @price = price
  end
end