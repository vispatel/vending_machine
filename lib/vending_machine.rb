class NotSupported < Exception; end
class VendingMachine
  COINS = [200, 100, 50, 20, 10, 5, 2, 1]

  attr_reader :product, :products

  def initialize(opts={})
    @running_total, @amount_due = 0, 0
    @products = opts[:products] || []
    @product = nil
  end

  def add_coin(money)
    raise TypeError unless money.is_a?(Money)
    raise NotSupported unless COINS.include?(money.value)
    @running_total += money.value
    dispense_product if amount_due.value <= 0
  end

  def running_total
    Money.new(@running_total)
  end

  def amount_due
    Money.new(@amount_due - @running_total)
  end

  def change
    return if amount_due.value >= 0
    change = -amount_due.value
    change = COINS.map do |coin|
      n = change/coin
      change %= coin
      Array.new(n){ Money.new(coin)}
    end
    change.flatten
  end

  def select_product(product)
    @product = find_product(product)
    @amount_due = @product.price.value if @product
  end

  private

  def find_product(product)
    @products.detect{|p| p.id == product.id}
  end

  def dispense_product
    @products.delete_at(@products.index(@product) || @products.length)
  end

end