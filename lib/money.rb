class Money
  attr_reader :value
  def initialize(value)
    @value = value
  end

  def ==(other_money)
    @value == other_money.value
  end
end