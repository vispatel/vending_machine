require 'simplecov'
SimpleCov.start

require 'vending_machine'
require 'money'
require 'product'

describe VendingMachine do

  let(:one_pence){Money.new(1)}
  let(:two_pence){Money.new(2)}
  let(:five_pence){Money.new(5)}
  let(:ten_pence){Money.new(10)}
  let(:twenty_pence){Money.new(20)}
  let(:fifty_pence){Money.new(50)}
  let(:one_pound){Money.new(100)}
  let(:two_pound){Money.new(200)}
  let(:five_pound){Money.new(500)}

  let(:coke){Product.new(1, "Coke", fifty_pence)}
  let(:fanta){Product.new(2, "Fanta", fifty_pence)}
  let(:sprite){Product.new(3, "Sprite", twenty_pence)}
  let(:water){Product.new(4, "Water", twenty_pence)}

  let(:vending_machine) {
    VendingMachine.new(
      products: [coke, fanta, water, coke, water],
      coins: [ten_pence, twenty_pence, twenty_pence, twenty_pence, fifty_pence, one_pound]
    )
  }

  describe "initial state of vending machine" do
    it "starts with zero" do
      vending_machine.running_total.should eq Money.new(0)
    end
  end

  describe "accepting supported coins" do
    it "accepts one pence coins" do
      vending_machine.add_coin(one_pence)
      vending_machine.running_total.should eq one_pence
    end
    it "accepts two pence coins" do
      vending_machine.add_coin(two_pence)
      vending_machine.running_total.should eq two_pence
    end
    it "accepts five pence coins" do
      vending_machine.add_coin(five_pence)
      vending_machine.running_total.should eq five_pence
    end
    it "accepts ten pence coins" do
      vending_machine.add_coin(ten_pence)
      vending_machine.running_total.should eq ten_pence
    end
    it "accepts twenty pence coins" do
      vending_machine.add_coin(twenty_pence)
      vending_machine.running_total.should eq twenty_pence
    end
    it "accepts fifty pence coins" do
      vending_machine.add_coin(fifty_pence)
      vending_machine.running_total.should eq fifty_pence
    end
    it "accepts one pound coins" do
      vending_machine.add_coin(one_pound)
      vending_machine.running_total.should eq one_pound
    end
    it "accepts two pound coins" do
      vending_machine.add_coin(two_pound)
      vending_machine.running_total.should eq two_pound
    end
  end

  describe "what happens when there are problems with the money given" do
    it "rejects coins that are not supported" do
      expect{vending_machine.add_coin(five_pound)}.to raise_error(NotSupported)
    end

    it "rejects the wrong type of input" do
      expect{vending_machine.add_coin('abc')}.to raise_error(TypeError)
    end
  end

  it "keeps a running total of coins inserted" do
    vending_machine.add_coin(ten_pence)
    vending_machine.add_coin(twenty_pence)
    vending_machine.add_coin(twenty_pence)
    vending_machine.running_total.should eq fifty_pence
  end

  it "allows selection of product" do
    vending_machine.select_product(coke)
    vending_machine.product.should eq coke
  end

  it "does not allow selection of products that are out of stock" do
    vending_machine.select_product(sprite)
    vending_machine.product.should eq nil
  end

  it "requests amount when product is selected" do
    vending_machine.select_product(coke)
    vending_machine.amount_due.should eq coke.price
  end

  it "requests more money if full amount is not given" do
    vending_machine.select_product(coke)
    vending_machine.add_coin(twenty_pence)
    vending_machine.add_coin(twenty_pence)
    vending_machine.amount_due.should eq ten_pence
  end

  it "dispenses product when full amount is taken" do
    vending_machine.select_product(coke)
    vending_machine.add_coin(twenty_pence)
    vending_machine.add_coin(twenty_pence)
    vending_machine.add_coin(ten_pence)
    vending_machine.products.size.should eq 4
  end

  describe "how much change is given when purchasing coke" do
    it "dispenses fifty pence when one pound is given" do
      vending_machine.select_product(coke)
      vending_machine.add_coin(one_pound)
      vending_machine.change.should eq [fifty_pence]
    end

    it "dispenses one pound and fifty pence coins when two pound is given" do
      vending_machine.select_product(coke)
      vending_machine.add_coin(two_pound)
      vending_machine.change.should eq [one_pound, fifty_pence]
    end
  end
  it "should only dispense change from coins held in stock"
  it "allows reloading of products"
  it "allows reloading of coins"

end


